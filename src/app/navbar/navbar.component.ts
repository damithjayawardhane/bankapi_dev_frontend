import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  sdate:Date;
  stime:Date;
  logged : Boolean;
  constructor( private router:Router,private AuthService:AuthService) { }

  ngOnInit() {
    this.sdate=new Date();
    this.stime=new Date();
    this.loginCheck();
  }
  loginCheck(){
    let token = this.AuthService.getToken();
    if(token=='' || token==undefined || token==null) {
      this.logged =false;
    }
    else{
      this.logged =true;
    }
  }

  log_page(){
    this.router.navigate(['Account']);
  }

  logOut(){
    this.AuthService.Logout();
    this.router.navigate(['login']);
    this.logged = false;
  }

}
