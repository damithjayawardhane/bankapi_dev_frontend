import { Injectable } from "@angular/core";
import { AuthService } from 'src/app/services/auth.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observer, Observable } from 'rxjs';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authService.getToken() != undefined) {
            return true;

        } else {
            this.router.navigate(["profile"])
            return false;
        }
    }
}