import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes }from '@angular/router';
import { AccountComponent } from './account/account.component';
import { ProfileComponent } from './profile/profile.component';
import { PFormComponent } from './p-form/p-form.component';
import { AuthGuard } from 'src/shared/auth.guard';


const routes :Routes=[
  {path:'',redirectTo:'/profile',pathMatch:'full'},
  {path:"Account",component:AccountComponent,canActivate:[AuthGuard]},
  {path:"p_form",component:PFormComponent},
  {path:"p_form/:id",component:PFormComponent},
  {path:"profile",component:ProfileComponent},
  {path:"**",component:ProfileComponent,pathMatch:'full'}
  
  
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})

export class AppRoutingModule { }
